# smooth-scroll-handler
Smooth mouse scroll.  
Package based on Manuel Otto's answer to this post:
https://stackoverflow.com/a/47206289/9006591

It fixes a bug with the original code, where the page scrolled back to its original position if you used the arrow keys to scroll, or if you dragged the scrollbar up or down.  
It also provides useful methods, events, properties and options.

> ⚠️ **Warning**: Scroll-hijacking can have a negative impact on the user experience and is therefore not suitable for all projects.

## Usage
```javascript
import ScrollHandler from 'smooth-scroll-handler';

const scrollHandler = new ScrollHandler();

function loop() {
    requestAnimationFrame(loop);
    
    scrollHandler.update();
}
```
That's all it takes, but you can do much more!

## Options
Options should be provided as one object.

### speed
How fast the page goes as the user scrolls.  
Default is `1`.


### smooth
A higher value means a smoother scroll.  
Default is `10`, minimum value is 1 (meaning no smoothing).

```javascript
import ScrollHandler from 'smooth-scroll-handler';

const scrollHandler = new ScrollHandler({ speed: 1, smooth: 10 });
```

## Methods

### disable( allowScroll = false, noScrollApproach = 'fixed' )
Deactivate the instance.  
This `allowScroll` option is false by default, the page scroll will be blocked. 
If this option is set to true, the native scroll (without smoothing) will be set back to normal.  
There's more than one way to block the scroll, so the `noScrollApproach` option lets you decide which approach to use. 
By default the `fixed` approach is used, meaning the documentElement position will be set to fixed. But you can also try the `overflow` approach, meaning an `overflow: hidden` will be applied on the documentElement.

### enable()
Reactivate the instance, meaning the scroll will be reactivated and smoothed again.

### scrollTo(destination, options)
```javascript
let options = {
    duration: 1,
    // Duration of the animation in milliseconds.
    // Default is .5

    ease: 'power2.out',
    // GSAP easing function (https://greensock.com/docs/v3/Eases)
    // Default is 'power2.inOut'

    allowScrollInterruption: false,
    // Defines if the scrollTo animation can be interrupted by a manual scroll
    // in the opposite direction.
    // Default is false

    offset: 200
    // Offset of the scrollTo stop position, from the target element.
    // A positive value will stop the scrollTo position above the target element.
    // Can be usefull when the page contains a sticky header, for instance.
    // Default is 0
}
scrollHandler.scrollTo('#somewhere', options);
// or
scrollHandler.scrollTo(200, options);
```

### handleNoScrollElements()
If elements with the `data-no-smooth-scroll` attribute are added asynchronously to the DOM, this method can be used to manually parse the DOM for those elements.


## Events
```javascript
scrollHandler.onStart.add(() => {
    console.log('scroll started');
});

scrollHandler.onStop.add(() => {
    console.log('scroll stopped');
});

scrollHandler.onDirectionChange.add(direction => {
    console.log('scroll direction changed to: ', direction);
});
```

## Properties
### .active = (boolean)
If set to false, the instance will stop updating, meaning the mouse scroll events will no longer be smoothed, but you'll still be able to use the keyboard or the scrollbar.  
Default is true.

### .direction (read-only)
Returns 1 if the page scrolled last towards the bottom, and -1 if it scrolled towards the top.

### .lerpedDelta (read-only)
Returns the difference between the previous and the next scroll position. This is useful if you need to make an animation adapt to the scrolling speed.

## Disabling the smooth scroll

Use the `data-no-smooth-scroll` attribute on elements that needs to use an inner native scroll, like textareas, or drop-down menus.

```html
<textarea data-no-smooth-scroll></textarea>
```

## Todo

- Returning completion