import gsap from 'gsap';
import normalizeWheel from 'normalize-wheel';

export default class ScrollHandler {
    constructor(options = {}) {
        this.target = document.documentElement;

        this.windowHeight = window.innerHeight;
        window.addEventListener('resize', () => {
            this.windowHeight = window.innerHeight;
        });

        this.active = true;

        this.speed = options.speed ?? 1;
        this.smooth = Math.max(options.smooth, 1) || 10;

        this.moving = false;
        this.pos = window.pageYOffset;
        this.direction = 0;
        this.lerpedDelta = 0;
        // During a scrollTo tween, this.direction is not updated.
        // This could be improved, but in the meantime, scrollToDirection returns 1 or -1 depending on the direction of the tween.
        this.scrollToDirection = 0;
        this.time = Date.now();
        this.manuallyDisabled = false;

        this.scrolledRef = this.scrolled.bind(this);
        this.target.addEventListener('wheel', this.scrolledRef, { passive: false });

        // Listens when the scrollbar is dragged or when the arrow keys are used
        let scrollTimeout;
        window.addEventListener('scroll', (e) => {
            if (this.moving) {
                return;
            }
            clearTimeout(scrollTimeout);
            scrollTimeout = setTimeout(() => {
                this.pos = window.pageYOffset;
            }, 100);
        });

        this.scrollToAnim = null;

        // Defines if the scrollTo animation can be interrupted by a manual scroll in the opposite direction.
        this.scrollToInterruptionAllowed = false;

        this.onStartCallbacks = [];
        this.onStart = {
            add: this._onStartAdd.bind(this),
        };

        this.onStopCallbacks = [];
        this.onStop = {
            add: this._onStopAdd.bind(this),
        };

        this.onDirectionChangeCallbacks = [];
        this.onDirectionChange = {
            add: this._onDirectionChangeAdd.bind(this),
        };

        this.noScrollElements = [];
        this.handleNoScrollElements();
    }

    _onStartAdd(callback) {
        if (typeof callback != 'function') {
            throw new Error('[smooth-scroll-handler] onStart callbacks must be of type function.');
        }
        this.onStartCallbacks.push(callback);
    }

    _onStopAdd(callback) {
        if (typeof callback != 'function') {
            throw new Error('[smooth-scroll-handler] onStop callbacks must be of type function.');
        }
        this.onStopCallbacks.push(callback);
    }

    _onDirectionChangeAdd(callback) {
        if (typeof callback != 'function') {
            throw new Error(
                '[smooth-scroll-handler] onDirectionChange callbacks must be of type function.'
            );
        }
        this.onDirectionChangeCallbacks.push(callback);
    }

    // Private method
    _disable(allowScroll = false, approach = 'fixed') {
        if (this.disabled) {
            return;
        }
        this.disabled = true;

        this.active = false;
        this.moving = false;
        this.target.removeEventListener('wheel', this.scrolledRef, { passive: false });

        if (allowScroll === false) {
            this.injectStyleSheetOnce();

            if (approach == 'overflow') {
                document.documentElement.classList.add('scrollhandler-noscroll-overflow');
                document.body.classList.add('scrollhandler-noscroll-overflow');
            } else if (approach == 'fixed') {
                this.lastScrollTop = document.documentElement.scrollTop;
                document.documentElement.classList.add('scrollhandler-noscroll-fixed');
                document.documentElement.style.top = -this.lastScrollTop + 'px';
            }
            this.noScrollApproach = approach;
        }
    }

    // Public method
    disable(allowScroll = false, approach = 'fixed') {
        this.manuallyDisabled = true;
        this._disable(allowScroll, approach);
    }

    injectStyleSheetOnce() {
        // to avoid modifying or overriding existing styles,
        // the safest way is to use a class and a dedicated stylesheet.
        if (!document.head.querySelector('#scrollhandler-stylesheet')) {
            let ss = document.createElement('style');
            ss.id = 'scrollhandler-stylesheet';
            ss.type = 'text/css';
            ss.textContent = /* css */ `
            .scrollhandler-noscroll-overflow {
                overflow: hidden;
                height: auto;
            }
            .scrollhandler-noscroll-fixed {
                position: fixed;
                overflow-y: scroll;
                width: 100%;
                height: 100%;
            }
            .scrollhandler-noscroll-fixed body {
                height: auto;
                overflow-y: auto;
            }
            .no-touch-action {
                touch-action: none;
            }
            `;
            document.head.append(ss);
        }
    }

    enable() {
        this.disabled = false;
        this.active = true;
        this.target.addEventListener('wheel', this.scrolledRef, { passive: false });
        this.manuallyDisabled = false;

        if (this.noScrollApproach == 'overflow') {
            document.documentElement.classList.remove('scrollhandler-noscroll-overflow');
            document.body.classList.remove('scrollhandler-noscroll-overflow');
        }
        if (this.noScrollApproach == 'fixed') {
            document.documentElement.classList.remove('scrollhandler-noscroll-fixed');
            document.documentElement.scrollTop = this.lastScrollTop;
        }
        this.noScrollApproach = null;
    }

    scrolled(evt) {
        evt.preventDefault(); // disable default scrolling

        if (this.scrollToAnim) {
            if (!this.scrollToInterruptionAllowed) {
                return;
            } else {
                // scrollTo can only be interrupted with a scroll in the opposite direction
                if (this.scrollToDirection != Math.sign(evt.deltaY)) {
                    this._scrollToEnded();
                }
            }
        }

        let pixelRatio = window.devicePixelRatio || 1;
        let delta = normalizeWheel(evt).pixelY / pixelRatio;

        this.pos += delta * this.speed;
        this.pos = Math.max(
            0,
            Math.min(this.pos, this.target.scrollHeight - this.target.clientHeight)
        ); // limit scrolling

        if (this.pos > 0 && this.pos < this.target.scrollHeight - this.target.clientHeight) {
            this.request_update = true;

            if (!this.moving) {
                if (this.onStartCallbacks.length) {
                    this.onStartCallbacks.forEach((callback) => {
                        callback();
                    });
                }
            }
        }
    }

    update() {
        const now = Date.now();

        if (!this.active || !this.request_update) {
            this.time = now;
            return;
        }

        const time_delta = (now - this.time).toFixed(1);
        const fps60_time_delta = 16.7;
        const time_delta_factor = time_delta / fps60_time_delta;

        this.time = now;

        this.moving = true;

        // calculates delta
        const delta = this.pos - this.target.scrollTop;
        const sign = Math.sign(delta);
        if (Math.abs(delta) < 1) {
            this.lerpedDelta = 0;
        } else {
            this.lerpedDelta = delta / (this.smooth / time_delta_factor);
            this.lerpedDelta = Math.ceil(Math.abs(this.lerpedDelta)) * sign;
        }

        if (Math.abs(this.lerpedDelta) > 0) {
            // updates direction
            let new_dir = Math.sign(this.lerpedDelta);
            if (new_dir != this.direction && this.onDirectionChangeCallbacks.length) {
                this.onDirectionChangeCallbacks.forEach((callback) => {
                    callback(new_dir);
                });
            }
            this.direction = new_dir;

            // applies delta
            this.target.scrollTop += this.lerpedDelta;
        } else {
            this.target.scrollTop += this.direction;

            this.moving = false;
            if (this.onStopCallbacks.length) {
                this.onStopCallbacks.forEach((callback) => {
                    callback();
                });
            }

            this.request_update = false;
        }
    }

    scrollTo(destination, options = {}) {
        let duration = options.duration ?? 0.5;
        let offset = options.offset ?? 0;
        let ease = options.ease || 'power2.inOut';
        let warning = typeof options.warning == 'boolean' ? options.warning : true;
        this.scrollToInterruptionAllowed = options.allowScrollInterruption ?? false;

        if (duration >= 50 && warning) {
            console.warn(
                '[smooth-scroll-handler] The duration of the scrollTo method should be set in seconds, not in milliseconds. Use the "warning" option to turn this warning off if you know what you\'re doing: .scrollTo(destination, { warning: false });'
            );
        }

        let dest;
        if (typeof destination == 'string') {
            dest = document.querySelector(destination);
            if (!dest) {
                throw new Error(
                    '[smooth-scroll-handler] Can\'t find scrollTo target element "' +
                        destination +
                        '"'
                );
            } else {
                dest = dest.getBoundingClientRect().top + document.documentElement.scrollTop;
            }
        } else if (typeof destination == 'object') {
            dest = destination.getBoundingClientRect().top + document.documentElement.scrollTop;
        } else if (typeof destination == 'number') {
            dest = destination;
        }

        if (this.scrollToAnim) {
            this.scrollToAnim.kill();
            this.scrollToAnim = null;
        }

        this.active = false;
        this.lerpedDelta = 0;

        if (!this.scrollToInterruptionAllowed) {
            this.injectStyleSheetOnce();
            document.body.classList.add('no-touch-action'); // prevents touch devices from scrolling
        }

        let scrollTopDestination = dest - offset;
        this.scrollToDirection = Math.sign(scrollTopDestination - this.target.scrollTop);

        if (duration == 0) {
            gsap.set(this.target, {
                scrollTop: scrollTopDestination,
            });
            this._scrollToEnded();
        } else {
            this.scrollToAnim = gsap.to(this.target, {
                scrollTop: scrollTopDestination,
                duration,
                ease,
                onComplete: () => {
                    this._scrollToEnded();
                },
            });
        }
    }

    _scrollToEnded() {
        if (this.scrollToAnim) {
            this.scrollToAnim.kill();
        }
        this.scrollToAnim = null;
        this.pos = this.target.scrollTop;
        setTimeout(() => {
            document.body.classList.remove('no-touch-action');
            this.active = true;
            this.moving = false;
        }, 10);
    }

    handleNoScrollElements() {
        const elements = document.querySelectorAll('[data-no-smooth-scroll]');
        for (let i = 0; i < elements.length; i++) {
            let el = elements[i];
            if (!this.noScrollElements.includes(el)) {
                el.addEventListener('mouseenter', () => {
                    this._disable(true);
                });
                el.addEventListener('mouseleave', () => {
                    if (!this.manuallyDisabled) {
                        this.enable();
                    }
                });
                this.noScrollElements.push(el);
            }
        }
    }
}
